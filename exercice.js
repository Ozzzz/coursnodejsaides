var http = require('http');
var url = require('url');

var server = http.createServer(function(req, res) {
	

	var page = url.parse(req.url).pathname;

	switch (page) 
	{

	case "/":
		res.writeHead(200, {"Content-Type": "text/html"});
		res.write('Ceci est la page home');
		res.end();
		break;

	case "/contact":
		res.writeHead(200, {"Content-Type": "text/html"});
		res.write('Ceci est la page contactez nous');
		res.end();
		break;

	default:
		res.writeHead(404);
		res.write('404 PAGE NOT FOUND')
		res.end();

	}	


});
server.listen(8080);