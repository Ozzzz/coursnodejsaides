var express = require('express');

var app = express();
console.log('__dirname: '+__dirname);

// mideware de logging
app.use(function(req,res,next){
	console.log(Date.now() 
		+ ' ' + req.ip
		+ ' ' + req.method 
		+ ' ' + req.originalUrl);
	
	next();
});
app.use(function(req,res,next){
	console.log('pre routage');
	next();
});

// Indique que le dossier /public contient des fichiers statiques (middleware chargé de base)
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end("Hello");
});


app.get('/contact', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end("Contactez moi");
});

// parametres dans l'url
app.get('/profil/:id', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Profile n°' + req.params.id);
});

app.get('/profil/:id/details', function(req, res) {
	var names = ['bob','willy', 'fred'];
    res.render('profile.ejs', {id: req.params.id, names:names});
    console.log ('Rendu du profile');
});


app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.send(404, 'Page introuvable !');
});


app.listen(8080);